package com.ins.insboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsBootApplication.class, args);
    }

}
