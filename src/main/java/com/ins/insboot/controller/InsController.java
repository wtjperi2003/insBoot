package com.ins.insboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ins")
public class InsController {

    @GetMapping("/test")
    public String test(){
        return "hello";
    }
}
